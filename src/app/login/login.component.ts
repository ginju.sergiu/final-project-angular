import { Component, OnInit } from '@angular/core';
import {UserService} from "../service/user.service";
import {UserLoginDto} from "../model/userLoginDto";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string = '';
  passwordValue: string = '';

  constructor(private userService: UserService,
              private router: Router) { }

  ngOnInit(): void {
  }

  login(){
    const userLoginDto: UserLoginDto= {
      username: this.username,
      password: this.passwordValue
    };

    this.userService.login(userLoginDto).subscribe(respnse=>{
      alert('Login user');
      //asta imi arata dupa ce ma loghez cu un user existent si tot e ok ( aici ar trebui sa imi returneze cursurule nu modulele)
      this.router.navigate(['/our-module'])
    })
  }

}
