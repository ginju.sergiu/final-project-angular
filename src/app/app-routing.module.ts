import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ModuleListComponent} from "./module-list/module-list.component";
import {ModuleFormComponent} from "./module-form/module-form.component";
import {LoginComponent} from "./login/login.component";

const routes: Routes = [

  {
    path: 'our-module',
    component: ModuleListComponent
  },

  {
    path: 'create-module',
    component: ModuleFormComponent
  },
  {path: 'login', component: LoginComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
