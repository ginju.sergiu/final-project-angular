import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ModulService} from "../service/modul.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-module-form',
  templateUrl: './module-form.component.html',
  styleUrls: ['./module-form.component.css']
})
export class ModuleFormComponent implements OnInit {

  moduleForm = new FormGroup({
    name: new FormControl(''),
    description: new FormControl('')
  });

  constructor(private moduleService: ModulService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const submitMessage = 'User input\n' +
      'Name:' + this.moduleForm.get('name')?.value +'\n'+
      'Description:' + this.moduleForm.get('description')?.value;
    alert(submitMessage);

    const moduleDto = {
      name: this.moduleForm.get('name')?.value,
      description: this.moduleForm.get('description')?.value
    };
    this.moduleService.addModule(moduleDto).subscribe(response=>{
      this.router.navigate(['/our-module'])
    })
  }
}
