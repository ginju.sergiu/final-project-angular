import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {ModuleReturnDto} from "../model/moduleReturnDto";
import {ModuleCreateDto} from "../model/moduleCreateDto";
import {UserService} from "./user.service";


@Injectable({
  providedIn: 'root'
})
export class ModulService {

  //tot ce e IMPORTAT  trebuie adaugat si in app.module.ts -> la import
  private modelUrl = "/api/v1/module";
  private httpClient;

  constructor(httpClient: HttpClient,
              private userService: UserService) {
    this.httpClient = httpClient;
  }

  public getAll(): Observable<ModuleReturnDto[]> {

    // @ts-ignore
    return this.httpClient.get(this.modelUrl + "/getAll",this.userService.optionsWithAuthorizationHeader);
  }

  public addModule(module: ModuleCreateDto): Observable<any> {
    return this.httpClient.post(this.modelUrl + "/post", module, this.userService.optionsWithAuthorizationHeader);
  }

  public updateModule(module: ModuleCreateDto): Observable<ModuleCreateDto> {
    return this.httpClient.put<ModuleReturnDto>(`${this.modelUrl}/update`, module, this.userService.optionsWithAuthorizationHeader);
  }

  public findById(moduleId: number): Observable<ModuleReturnDto> {
    return this.httpClient.get<ModuleReturnDto>(`${this.modelUrl}/findById/${moduleId}`, this.userService.optionsWithAuthorizationHeader)

  }
}
