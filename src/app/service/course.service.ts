import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CourseCreateDto} from "../model/courseCreateDto";
import {CourseReturnDto} from "../model/courseReturnDto";


@Injectable({
  providedIn: 'root'
})
export class CourseService {

  private courseUrl: string;


  constructor(private httpClient: HttpClient) {
    this.courseUrl = "/api/v1/course";
  }

  // public createCourse(courseCreateDto: CourseCreateDto): Observable<any> {
  //   return this.httpClient.post(this.courseUrl, courseCreateDto);
  // }
  public getAll():Observable<CourseReturnDto[]>{
    return this.httpClient.get<CourseReturnDto[]>(this.courseUrl+"/getAll");
  }

  public addCourse(course: CourseCreateDto): Observable<CourseCreateDto> {
    return this.httpClient.post<CourseReturnDto>(`${this.courseUrl}/add`, course);
  }

}
