import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserReturnDto} from "../model/userReturnDto";
import {UserLoginDto} from "../model/userLoginDto";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userClient = '/api/v1/user';
  private userReturnDto: UserReturnDto | undefined;
  optionsWithAuthorizationHeader = {};

  constructor(private httpClient: HttpClient) {
  }

  getUser(): UserReturnDto {
    return <UserReturnDto>this.userReturnDto;
  }

  setUser(user: UserReturnDto) {
    this.userReturnDto = user;
  }

  // @ts-ignore
  public login(userLoginDto: UserLoginDto): Observable<UserReturnDto> {
    this.optionsWithAuthorizationHeader = {
      headers: {
        Authorization: 'Basic ' + btoa(userLoginDto.username + ':' + userLoginDto.password)
      }
    }
    return this.httpClient.post(this.userClient + '/login', userLoginDto, this.optionsWithAuthorizationHeader) as Observable<UserReturnDto>;
  }
}


