import { Component, OnInit } from '@angular/core';
import {ModulService} from "../service/modul.service";
import {ModuleReturnDto} from "../model/moduleReturnDto";

@Component({
  selector: 'app-module-list',
  templateUrl: './module-list.component.html',
  styleUrls: ['./module-list.component.css']
})
export class ModuleListComponent implements OnInit {

  moduleList: ModuleReturnDto[] = [];

  constructor(private moduleService: ModulService) {
    this.moduleService = moduleService;
  }

  ngOnInit(): void {
    this.moduleService.getAll().subscribe(successfulResponse => {
      console.log("Succesful response received: " + successfulResponse);
        this.moduleList = successfulResponse;
    })
  }

}
