import { Component, OnInit } from '@angular/core';
import {CourseReturnDto} from "../model/courseReturnDto";
import {CourseService} from "../service/course.service";



@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {


  courseList : CourseReturnDto[] = [];

  constructor(private courseService: CourseService) {
    this.courseService = courseService;
  }

  ngOnInit(): void {
    this.courseService.getAll().subscribe(successfulResponse=> {
      console.log("Succesful response received: " + successfulResponse);
      this.courseList = successfulResponse;
    })
  }

}
