import {ModuleReturnDto} from "./moduleReturnDto";


export class CourseCreateDto {
  name:String;
  startDate:String;
  endDate:String;
  listOfModule: ModuleReturnDto[];

  constructor(name: String, startDate: String, endDate: String, listOfModule: ModuleReturnDto[]) {
    this.name = name;
    this.startDate = startDate;
    this.endDate = endDate;
    this.listOfModule = listOfModule;
  }
}
