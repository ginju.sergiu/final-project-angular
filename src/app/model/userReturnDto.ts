export class UserReturnDto {
  userName: String;
  firstName: String;
  lastName: String;
  active: Boolean;
  type: String;


  constructor(userName: String, firstName: String, lastName: String, active: Boolean, type: String) {
    this.userName = userName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.active = active;
    this.type = type;
  }
}
